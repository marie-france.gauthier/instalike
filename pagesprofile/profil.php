<?php 
session_start();

include('../PHP/connexionbdd.php');

$userQuery = $bdd->query("SELECT * FROM users WHERE id=".$_GET["id"]);
$user = $userQuery->fetch();

$imageQuery = $bdd -> query('SELECT * FROM images WHERE id_users=' .$_GET["id"]);


?>

<!doctype html>
<html lang="fr">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Oxygen|Pacifico" rel="stylesheet">
    <title>InstaLike - <?=$user["name"]?></title>
</head>

<body>
    <nav class="navbar justify-content-between">
        <a class="navbar-brand titrenav col-2 col-sm-2">
            <h1>Insta Like©</h1>
        </a>

        <div class="row">
            <a href="../explorer.php">
                <img class="icons" src="../images/icons/navigation.svg" alt="voir les profils">
            </a>
            <!--<a href="#" class="ml-3">
                <img class="icons" src="../images/icons/like.svg" alt="notifications">
            </a>-->
            <a href="../page_compte.php" class="col">
                <img class="icons" src="../images/icons/user_account.svg" alt="compte perso">
            </a>
        </div>
    </nav>

    <div class="container-fluid header">
        <div class="row">
            <img class="rounded-circle image-perso justify-content-center" src="../images/<?=$user["avatar"]?>" alt="">     
        </div>

        <div class="card card_perso justify-content-center">
            <div class="card-body">
                <h3 id="pseudo_user"><?=$user["nickname"]?></h3>
                <p>6 publications</p>
                <p id="name_user"><?=$user["name"]?></p>
                <p id="bio"><?=$user["bio"]?></p>
            </div>
        </div>
    </div>

    <!-- images des modals -->
    <div class="container">
        <div class="row">
    <?php foreach($imageQuery as $image) { ?>
    
            <div class="col-sm-6 col-md-6 col-lg-4">
                <a class="" data-toggle="modal" href="#photo<?= $image['id'] ?>">
                    <img class="img-profil-modal col" src="../images/<?= $image['image'] ?>">
                </a>
            </div>
        

    <!-- Modal -->
    <div class="modal fade" id="photo<?= $image['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id=""></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img class="img-fluid" src="../images/<?= $image['image'] ?>">
                    
                    <p> <?php 
                        if (isset($image['description_image']) AND $image['description_image'] !== "") {
                            echo "<hr/>" . $image['description_image'];
                        } ?> </p>
                </div>
                <div class="modal-footer">
                    <span class="liking">
                        <?php
                        
                        $chemin_image = $bdd -> prepare('SELECT id FROM images WHERE image = ?');
                        $chemin_image -> execute(array($image["image"]));
                        $chemins = $chemin_image -> fetch();

                        // on compte le nombre de like de l'image cliqué
                        $nb_like = $bdd -> prepare('SELECT COUNT(liking) FROM interactions WHERE id_images = ?');
                        $nb_like -> execute(array($chemins['id']));
                        $total_like = $nb_like -> fetchColumn();

                        echo $total_like;
                        
                        ?>
                    </span>
                    <form action="../PHP/like.php?chemin_image=<?= $image['image'] ?>" method="post" onsubmit="storeLike(event, this)">    
                        <button type="submit" class="btn"><img class="icons"src="../images/icons/btn_like.svg"></button>
                    </form>    
                </div>
                <div id="disqus_thread"></div>
            </div>
        </div>
    </div>
    <?php } ?>
    </div>
    </div>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script>
        /**
        *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
        *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
        /*
        var disqus_config = function () {
        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        */
        (function() { // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        s.src = 'https://instalike.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
        })();
    </script>
    <script src="../js/like.js"></script>

</html>