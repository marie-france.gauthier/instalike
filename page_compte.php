<?php

include 'PHP/display.php';

?>
<!doctype html>
<html lang="fr">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Oxygen|Pacifico" rel="stylesheet">
    <title>InstaLike - page d'accueil</title>
</head>

<body>
    <nav class="navbar justify-content-between">
        <a class="navbar-brand titrenav col-2 col-sm-2">
            <h1>Insta Like©</h1>
        </a>
        <div class="row">
            <a href="explorer.php">
                <img class="icons" src="images/icons/navigation.svg" alt="voir les profils">
            </a>
            <!-- <a href="#" class="ml-3">
                <img class="icons" src="images/icons/like.svg" alt="notifications">
            </a>-->
            <a href="#" class="col">
                <img class="icons" src="images/icons/user_account.svg" alt="compte perso">
            </a>
        </div>
    </nav>

    <div class="container-fluid header">
        <div class="row">
            <img class="rounded-circle image-perso justify-content-center" src="#" alt="">     
        </div>

        <div class="card card_perso justify-content-center">
            <div class="card-body">
                <h3 id="pseudo_user">
                    <?php
                    echo $_SESSION['nickname']?>
                </h3>
                <p>0 publications</p>
                <p id="name_user"><?php
                echo $_SESSION['name']?></p>
                <p contenteditable="true" id="bio"> <?php
                if (isset($_SESSION['bio'])) { echo $_SESSION['bio']; }?> </p>
                <button type="button" class="btn btn-bio" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Modifier</button>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Nouvelle description</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="PHP/bio.php" method="post">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="message-text" class="col-form-label">Description:</label>
                                        <textarea class="form-control" id="message-text" name="bio"></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                    <button type="submit" class="btn btn-primary">Valider</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <form action="PHP/logout.php">
                    <button type="submit" class="btn btn-danger">Déconnexion</button>
                </form>
            </div>
        </div>
    </div>

    <!-- images des modals -->
    <!-- <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-4">
                <a class="" data-toggle="modal" href="#">
                    <img class="img-modal col" src="">
                </a>
            </div>
        </div>
    </div> -->

    <!-- Modal -->
    <!-- modal-image 1 -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="js/image.js"></script>
</body>

</html>