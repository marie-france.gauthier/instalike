<?php 

    include('PHP/connexionbdd.php');

    $profils = $bdd->query('SELECT * FROM users WHERE avatar <> ""');

?>

<!doctype html>
<html lang="fr">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Oxygen|Pacifico" rel="stylesheet">
    <title>InstaLike - page d'accueil</title>
</head>

<body>
    <!--Barre de navigation-->
    <nav class="navbar justify-content-between">
        <a class="navbar-brand titrenav col-2 col-sm-2">
            <h1>Insta Like©</h1>
        </a>

        <div class="row">
            <a href="#">
                <img class="icons" src="images/icons/navigation.svg" alt="voir les profils">
            </a>
            <!--<a href="#" class="ml-3">
                <img class="icons" src="images/icons/like.svg" alt="notifications">
            </a>-->
            <a href="page_compte.php" class="col">
                <img class="icons" src="images/icons/user_account.svg" alt="compte perso">
            </a>
        </div>
    </nav>

    <div class="container header container-exp">
        <div class="row col">
        <?php foreach($profils as $profil) { ?>
            <div class="col d-flex justify-content-around">
                <!--Premier profile selectionnable-->
                <a href="pagesprofile/profil.php?id=<?=$profil["id"]?>">
                    <div class="justify-content-center">
                        <img class="rounded-circle image-perso justify-content-center" src="images/<?= $profil["avatar"] ?>" alt="photo profile">
                    </div>
                    <h3 class="explorer text-center"><?=$profil["name"]?></h3>
                </a>     
                
            </div>
        <?php } ?>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="js/image.js"></script>
</body>

</html>