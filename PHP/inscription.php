<?php
session_start();
$_SESSION['email'] = $_POST['email'];
$_SESSION['nickname']= $_POST['nickname'];
$_SESSION['name']=$_POST['name'];


// Connexion à la base de données
include 'connexionbdd.php';

//l'email est-il déjà utilisée
$email_exist = $bdd -> prepare('SELECT COUNT(*) FROM users WHERE email = ?');
$email_exist -> execute(array($_POST['email']));

// si oui ne pas enregistrer les données dans la bdd
if ($email_exist -> fetchColumn() >= '1') {
    header('Location: ../index.php?email_use');
} else { 

    //sécurisation du mot de passe
    $hash_mdp = password_hash($_POST['password'], PASSWORD_DEFAULT);

    // Sinon insertion des données d'inscription dans la bdd
    $req = $bdd->prepare('INSERT INTO users (name, nickname, password, email) VALUES(?, ?, ?, ?)');

    $req->execute(array($_POST['name'],$_POST['nickname'], $hash_mdp ,$_POST['email']));

        // Vérifions si la requête a fonctionné
        if(!$req) {
            // terminer le programme avec l'erreur d'affichée
                die($bdd->errorInfo()[2]); 
            }
    include 'perso.php';
    header('Location: ../page_compte.php');
}

setcookie('email', $_POST['email'], time() + 365*24*3600, '/', null, false, true);
setcookie('name', $_POST['name'], time() + 365*24*3600, '/', null, false, true);
setcookie('nickname', $_POST['nickname'], time() + 365*24*3600, '/', null, false, true);



?>