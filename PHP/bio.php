<?php
session_start();
include 'connexionbdd.php';

$bio = $bdd->prepare('UPDATE users SET bio = ? WHERE email=?');
$bio->execute(array($_POST['bio'], $_SESSION['email']));

    $description = $bdd -> prepare('SELECT bio FROM users WHERE email = ?');
    $description->execute(array($_SESSION['email']));

    $bio_description = $description -> fetch();

    $_SESSION['bio'] = $bio_description['bio'];

header('Location: ../page_compte.php');
