<?php
session_start();

include '../PHP/connexionbdd.php';

// print_r(($_POST['chemin_image'])) . "</br>";

//récupération de l'id de l'image liker
$id_images = $bdd -> prepare('SELECT id FROM images WHERE image = ?');

$id_images -> execute(array($_GET['chemin_image']));

// pour afficher une erreur sql sur un requête préparé
// print_r($id_images->debugDumpParams());
// exit;

$id_image = $id_images -> fetch();
// print_r($id_image);

//vérifié si l'email de l'utilisateur a déjà like cette photo (on compte si l'email et l'id_image est déjà dans la bdd)
$like_exist = $bdd -> prepare('SELECT COUNT(*) FROM interactions WHERE email= ? AND id_images = ?');
$like_exist -> execute(array($_SESSION['email'], $id_image['id']));

//On check s'il y a erreur
if(!$like_exist){
    print_r($id_images->debugDumpParams());
}

//On récupère le nb de ligne où "email" a liké l'image
$liking = $like_exist -> fetchColumn();
// print_r($liking);

//Si pas liké l'image
if($liking == 0) {

    //on insère le like dans la bdd
    $like = $bdd -> prepare('INSERT INTO interactions (email, liking, id_images) VALUES (?,?,?)');
    $like -> execute(array($_SESSION['email'], 1, $id_image['id']));

} 

//on compte le nombre de like de l'image cliqué
$nb_like = $bdd -> prepare('SELECT COUNT(liking) FROM interactions WHERE id_images = ?');
$nb_like -> execute(array($id_image['id']));
$total_like = $nb_like -> fetchColumn();

echo $total_like;
?>







