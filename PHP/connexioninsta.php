<?php
session_start();

include 'connexionbdd.php';
// Récupération de l'email et du mdp en bdd
//sélection dans la bdd où email = $_POST
include 'perso.php';

        // $recup_connection = $bdd -> prepare('SELECT email, password, name, nickname FROM users WHERE email = ?');
        // $recup_connection -> execute(array($_POST['email']));
        // $result = $recup_connection -> fetch();

//comparaison mdp haché avec celui du formulaire
$ifPasswordCorrect = password_verify($_POST['password'], $user['password']);

if (!$user) {
    header('Location: ../index.php?wrong');
    // echo 'mauvais email ou mauvais password!';
} else {
    if ($ifPasswordCorrect) {
        $_SESSION['bio']= $user['bio'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['name'] = $user['name'];
        $_SESSION['nickname'] = $user['nickname'];
        
        header('Location: ../page_compte.php');
    } else {
        header('Location: ../index.php?password_wrong');
        //echo 'mauvais mot de passe !';

    }
}
//récupérer l'email lors d'une nouvelle connexion
setcookie('email', $_POST['email'], time() + 365*24*3600, '/', null, false, true);