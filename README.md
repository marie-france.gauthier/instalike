Premier Projet (projet front et back-end) de la formation développeur web de Simplon réalisé avec la collaboration de Kevin Sinjy


Réalisation d'un résau social en HTML, CSS, PHP7 et MySQL


InstaLike est composé d'une interface en HTML, CSS et Javascript.
Les messages sont dynamiquement envoyés et affichés en PHP grâce la base de données MySQL.

Utilisation d'un framework front-end

Bootstrap a été utilisé pour réalisé le site InstaLike

Utiliser les requêtes AJAX pour éviter le rechargement de page

InstaLike utilise une requête ajax pour afficher le nombre de "like".

Les données sont dynamiquement générées par PHP avec des requêtes SQL. 

http://projets.simplon-roanne.com/InstaLike/