<?php
session_start();

?>
<!doctype html>
<html lang="fr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <link href="https://fonts.googleapis.com/css?family=Oxygen|Pacifico" rel="stylesheet">

  <title>Insta Like - page d'accueil</title>
</head>

<body>

  <h1 class="titre">Insta Like©</h1>

  <div class="accordion col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-4 offset-lg-4" id="accordionExample">
    <!-- card inscription -->
    <div class="card accueil">
      <div class="card-header" id="headingOne">
        <h5 class="mb-0">
          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            Inscription
          </button>
        </h5>
      </div>
      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
        <div class="card-body">

          <!-- formulaire d'inscription' -->
          <form action="PHP/inscription.php" method="post">
            <div class="form-group">
            
            
            <!--message d'alerte-->
        <small id="passwordHelp" class="text-danger">
        <?php if (isset($_GET['email_use'])) {
                echo "Cet email est déjà utilisé, choisissez en un autre"; } ?>
        </small>      
      
        <input type="email" required class="form-control <?php if (isset($_GET['email_use'])) {?> is-invalid <?php } ?>" name="email" aria-describedby="emailHelp" placeholder="Email" value="<?php 
                if (isset($_GET['email_use']) AND isset($_COOKIE["email"])) {
                  echo $_COOKIE["email"]; 
                } ?> ">
            </div>
            <div class="form-group">
              <input type="text" required class="form-control" name="name" aria-describedby="emailHelp" placeholder="Nom complet" value="<?php 
                if (isset($_GET['email_use']) AND isset($_COOKIE["name"])) {
                  echo $_COOKIE["name"]; 
                } ?>">
            </div>
            <div class="form-group">
              <input type="text" required class="form-control" name="nickname" aria-describedby="emailHelp" placeholder="Pseudo" value="<?php 
                if (isset($_GET['email_use']) AND isset($_COOKIE["nickname"])) {
                  echo $_COOKIE["nickname"];
                } ?>">
            </div>
            <div class="form-group">
              <input type="password" required class="form-control" name="password" placeholder="Mot de passe">
            </div>
            <button type="submit" class="btn btn-next">Suivant</button>
          </form>


        </div>
      </div>
    </div>
    <!-- card connexion -->
    <div class="card">
      <div class="card-header" id="headingTwo">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
            aria-controls="collapseTwo">
            Connexion
          </button>
        </h5>
      </div>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
        <div class="card-body">

          <!-- formulaire de connexion -->
          <form action="PHP/connexioninsta.php" method="post">
            <div class="form-group">
              <!--message d'erreur-->
              <small class="text-danger">
              <?php if (isset($_GET['wrong'])) {
                echo "mauvais identifiant ou mot de passe"; } ?>
        </small>   
              <input type="email" class="form-control <?php if (isset($_GET['wrong'])) {?> is-invalid <?php } ?>" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email" name="email" value="<?php  if(isset($_COOKIE["email"])){echo $_COOKIE["email"];}?>">
            </div>
            <div class="form-group">
              <!--Message d'erreur-->
            <small class="text-danger">
              <?php if (isset($_GET['password_wrong'])) {
                echo "mauvais mot de passe"; } ?>
        </small> 
              <input type="password" class="form-control <?php if (isset($_GET['password_wrong']) OR isset($_GET['wrong'])) {?> is-invalid <?php } ?>" id="exampleInputPassword1" placeholder="Mot de passe" name="password">
            </div>
            <button type="submit" class="btn btn-next">Se connecter</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>